#! /usr/bin/python

import re
import os
import shutil

logfile=open("hosts.real","r")
IPADD=open("IPaddress.txt","w")
new=open("host.new","w")

commentRegex=re.compile("#", re.IGNORECASE)

for line in logfile:
    if commentRegex.search(line):
        line=line.rstrip("\n\r")
        line=re.sub("#.*","",line)
    #print (line)

    data=line.split()
    try:
        line=line.rstrip("\n\r")
        MACaddress=data.pop()
        print(MACaddress)
    except:
        pass

    line=re.sub('\s+',' ', line).strip()
    new.write(line+"\n")
    print(line)


    try:
        file=line.split()
        IP=file.pop(0) + "\n"
        IPADD.write(IP)
    except:
        pass


logfile.close()
IPADD.close()
new.close()

os.rename("host.new","host.real")
